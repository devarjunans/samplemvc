using Sitecore.ContentSearch;
using System.Collections.Generic;
using System.Linq;

using LuceneSearch.Models;

namespace LuceneSearch.Helpers
{
    public class SearchHelper
    {
        static string indexName = "Lucene_Master";
        public static IProviderSearchContext IndexSearchContext
        {
            get
            {
                return ContentSearchManager.GetIndex(indexName).CreateSearchContext();
            }
        }
        public static List<SearchResultViewModel> SearchBy(string SearchTerm)
        {

            List<SearchResultViewModel> searchResults = new List<SearchResultViewModel>();
            using (var context = IndexSearchContext)
            {
                var results = context.GetQueryable<ProductSearchResultItem>()
                    .Where(result => result.Name.Contains(SearchTerm) || result.Description.Contains(SearchTerm)).ToList();

                foreach (var result in results)
                {
                    SearchResultViewModel searchResult = new SearchResultViewModel();

                    searchResult.Name = result.Name;
                    searchResult.Description = result.Description;
                    searchResult.ImageUrl = result.ImageUrl;

                    searchResults.Add(searchResult);
                }

                return searchResults;
            }
        }
        public static List<string> GetSuggestions()
        {
            List<string> keywordsList = new List<string>();

            using (var context = IndexSearchContext)
            {
                var keywords = context.GetQueryable<ProductSearchResultItem>().AsQueryable().Select(i => i.Name);

                foreach (var keyword in keywords)
                {
                    if (!keywordsList.Contains(keyword))
                    {
                        keywordsList.Add(keyword);
                    }
                }
            }

            return keywordsList;
        }
    }
}