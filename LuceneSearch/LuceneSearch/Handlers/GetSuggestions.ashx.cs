﻿using LuceneSearch.Helpers;
using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json;

namespace LuceneSearch.Handlers
{
    public class GetSuggestions : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string jsonData = ConvertToJson(SearchHelper.GetSuggestions());
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(jsonData);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private string ConvertToJson(List<string> suggestions)
        {
            return JsonConvert.SerializeObject(suggestions);
        }
    }
}