﻿<%@ control language="C#" autoeventwireup="true" codebehind="SearchResult.ascx.cs" inherits="LuceneSearch.Layouts.SearchResult" %>


<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Lucene Search</a>
        </div>

        <div class="navbar-form navbar-right" role="search">
            <div class="form-group">
                <asp:textbox id="txtSearch" runat="server" class="form-control" placeholder="Search" />
            </div>
            <asp:button id="btnSubmit" runat="server" text="submit" class="btn btn-default" onClick="BtnSubmit_Click" />
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">
   <div class="row" runat="server">
        <asp:repeater id="repeaterSearchResultList" runat="server" onitemdatabound="RepeaterSearchResultList_ItemDataBound">
          <itemtemplate>
            <div class="col-md-4 portfolio-item">
                <a href="#">
                   <asp:Image id="imageURL" class="img-responsive" runat="server" style="height:200px;width:360px"></asp:Image>
                </a>
                <h3>
                    <asp:Label id="name" runat="server"></asp:Label>
                </h3>
                <p><asp:Label id="description" runat="server"></asp:Label></p>
            </div>
        </itemtemplate>
        </asp:repeater>
    </div>
</div>

