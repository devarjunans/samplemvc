﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Sites;

namespace LuceneSearch.ComputedFields
{
    public class ImageUrlComputedField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            string mediaURL = string.Empty;
            Item item = indexable as SitecoreIndexableItem;
            if (item == null || item["Image"] == null)
                return null;

            ImageField imageField = item.Fields["Image"];
            if (imageField != null)
            {
                var mediaItem = imageField.MediaItem;
                if (mediaItem != null)
                    mediaURL = MediaManager.GetMediaUrl(mediaItem);

            }

            return mediaURL;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}