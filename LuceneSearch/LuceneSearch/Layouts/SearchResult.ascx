﻿<%@ control language="C#" autoeventwireup="true" codebehind="SearchResult.ascx.cs" inherits="LuceneSearch.Layouts.SearchResult" %>


<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Lucene Search</a>
        </div>

        <div class="navbar-form navbar-right" role="search">
            <div class="form-group">
                <asp:textbox id="txtSearch" runat="server" class="form-control" placeholder="Search" ClientIDMode="Static"/>
            </div>
            <asp:button id="btnSubmit" runat="server" text="submit" class="btn btn-default" onClick="BtnSubmit_Click" />
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">
   <div class="row" runat="server">
        <asp:repeater id="repeaterSearchResultList" runat="server" onitemdatabound="RepeaterSearchResultList_ItemDataBound">
          <itemtemplate>
            <div class="col-md-4 portfolio-item">
                <a href="#">
                   <asp:Image id="imageURL" class="img-responsive" runat="server" style="height:200px;width:360px"></asp:Image>
                </a>
                <h3>
                    <asp:Label id="name" runat="server"></asp:Label>
                </h3>
                <p><asp:Label id="description" runat="server"></asp:Label></p>
            </div>
        </itemtemplate>
        </asp:repeater>
   </div>

    <div class="row">
    <div class="panel-group" id="accordion">
        <div class="col-md-3 portfolio-item">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                  <a data-toggle="collapse" href="#facets-1"><span class="glyphicon glyphicon-tag"></span> Taille</a>
                </h4>
                </div>
                <div id="facets-1" class="panel-collapse collapse in">
                    <ul class="list-group">
                        <div class="list-group-item checkbox checkbox-circle">
                            <input type="checkbox" name="size" id="size1" />
                            <label for="size1">100 Ko à 1 Mo</label>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
    
</div>


