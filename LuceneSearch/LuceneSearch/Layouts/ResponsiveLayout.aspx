﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResponsiveLayout.aspx.cs" Inherits="LuceneSearch.Layouts.ResponsiveLayout" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Power of Lucene Search</title>
    
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/darkly/bootstrap.min.css" rel="stylesheet">
    <link href="css/Search Styles/3-col-portfolio.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <form id="form1" runat="server">
    <sc:placeholder key="main-layout" runat="server" />
    <script src="js/Search Scripts/jquery.js"></script>
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="js/Search Scripts/bootstrap.min.js"></script>
	<script src="js/Search Scripts/search.js"></script>
    </form>
</body>

</html>
