﻿using System;
using System.Web.UI.WebControls;
using LuceneSearch.Helpers;
using LuceneSearch.Models;

namespace LuceneSearch.Layouts
{
    public partial class SearchResult : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                var result = SearchHelper.SearchBy(txtSearch.Text);

                repeaterSearchResultList.DataSource = result;
                repeaterSearchResultList.DataBind();
            }
        }

        protected void RepeaterSearchResultList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var searchResult = (SearchResultViewModel)e.Item.DataItem;

                if (searchResult != null)
                {
                    var name = (Label)e.Item.FindControl("name");
                    if (name != null)
                        name.Text = searchResult.Name;

                    var description = (Label)e.Item.FindControl("description");
                    if (description != null)
                        description.Text = searchResult.Description;

                    var imageURL = (Image)e.Item.FindControl("imageURL");
                    if (imageURL != null)
                        imageURL.ImageUrl = searchResult.ImageUrl;
                }
            }
        }

    }
}