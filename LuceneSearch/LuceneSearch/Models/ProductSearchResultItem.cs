﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;

namespace LuceneSearch.Models
{
    public class ProductSearchResultItem : SearchResultItem
    {
        [IndexField("Name")]
        public string Name { get; set; }

        [IndexField("Description")]
        public string Description { get; set; }

        [IndexField("ImageUrl")]
        public string ImageUrl { get; set; }
    }
}