﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;

namespace LuceneSearch.Models
{
    public class SearchResultViewModel 
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }
    }
}